/* test comment continuously *//* test comment continuously */ ///

// test single line comment   ####

/*test multiline comment in one line*/

/* test single line comment follow multiline comment *///test test

/* test single line comment follow multiline comment */ //test test

/*
test for multiline comment
*/

//* /*

/*
test for multiline comment*/

/*
test for multiline comment and follow valid code */ #define FLAG1 0

#define FLAG2

#undef FLAG2

#if define FLAG1
#elif define FLAG2
#else
#ifndef FLAG2
#endif
#endif 

#ifdef FLAG1
#endif

#error ERROR

#pragma PLUSH

#include <stdio.h>

int VAR1;

int VAR2 = 0; // Test single line comment follow

int VAR3; /* test multi comment follow
*/

int array[] = 
{
    1, // single comment
    2, /* multiline comment */
    3
};

void func1();

void func1()
{
    printf("function 1 line 1"); // single comment

    printf("function 1 line 2"); // single comment
}

void func2(){
    printf("function 2 line 1"); // single comment

    printf("function 2 line 2"); // single comment

    if(True){
        printf("function 2 line 2"); // single comment
    }
    else if (False) {
        printf("function 2 line 2"); // single comment
    }
    else
    {
        printf("function 2 line 2"); // single comment
    }
}

int main(){

    return True
}