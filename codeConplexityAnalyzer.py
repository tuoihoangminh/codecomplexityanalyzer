#!/usr/bin/env python

import sys, os, shutil

inputFilePath = '.\\test1.c'
TEMP_DIR = '.\\temp'
TEMP_FILE_COUNT = 0
MODULES = []
C_PREPROCESSOR = [
    '#define',
    '#include',
    '#undef',
    '#ifdef',
    '#ifndef',
    '#if',
    '#else',
    '#elif',
    '#endif',
    '#error',
    '#pragma'
]
IS_COMMENTED = False

def getCodeText(line):
    global IS_COMMENTED
    oneLineSymbolIndex = line.find('//')
    multiLineSymbolIndex = line.find('/*')
    if oneLineSymbolIndex == -1 and multiLineSymbolIndex == -1:
        codeText = line
    elif oneLineSymbolIndex == -1 and multiLineSymbolIndex != -1:
        endMultiLineSymbol = line.find('*/')
        codeText = line[:multiLineSymbolIndex]
        if endMultiLineSymbol != -1:
            codeText += line[endMultiLineSymbol + 2 :]
        else:
            IS_COMMENTED = True
    elif oneLineSymbolIndex != -1 and multiLineSymbolIndex == -1:
        codeText = line[:oneLineSymbolIndex]
    elif multiLineSymbolIndex < oneLineSymbolIndex:
        endMultiLineSymbol = line.find('*/')
        codeText = line[:multiLineSymbolIndex]
        if endMultiLineSymbol != -1:
            codeText += line[endMultiLineSymbol + 2 :]
        else:
            IS_COMMENTED = True
    else:
        codeText = line[:oneLineSymbolIndex]
    #print(codeText)
    return codeText

def removeAllComments(inputFilePath, outputFilePath):
    global IS_COMMENTED
    try:
        inFile = open(inputFilePath, 'r')
    except:
        sys.exit('Cannot read file ' + inputFilePath)
    
    try:
        outFile = open(outputFilePath, 'w')
    except:
        sys.exit('Cannot create file ' + outputFilePath)

    for line in inFile:
        codeText = line
        while codeText.find('//') != -1 or codeText.find('/*') != -1:
            if IS_COMMENTED == False:
                codeText = getCodeText(codeText)
            else:
                endMultiLineIndex = codeText.find('*/')
                if endMultiLineIndex != -1:
                    print('set FALSE')
                    IS_COMMENTED = False
                    codeText = codeText[endMultiLineIndex + 2 :]
                    codeText = getCodeText(codeText)
                else:
                    codeText = ''
            #print(codeText)
            
        for c in ['\r', '\n']:
            codeText = codeText.replace(c, '')
        
        is_empty = True
        for c in codeText:
            if c != ' ' and c != '\t':
                is_empty = False
                break
        if is_empty:
            codeText = ''

        print(IS_COMMENTED)
        if len(codeText) != 0:
            print(codeText)
        else:
            print('empty')

    inFile.close()
    outFile.close()

if __name__ == "__main__":

    """
    Remove temp folder if exist
    """
    if os.path.exists(TEMP_DIR):
        shutil.rmtree(TEMP_DIR)
    
    """
    Create temp dir
    """
    os.mkdir(TEMP_DIR)

    tempFilePath = TEMP_DIR + '\\' + str(TEMP_FILE_COUNT) + '.tmp'
    TEMP_FILE_COUNT += 1

    removeAllComments(inputFilePath, tempFilePath)

    module = (inputFilePath, tempFilePath)
    MODULES.append(module)

    print(MODULES)

    sys.exit()